#!/usr/bin/env python
charFilter = [".",",","!","(",")","[","]"," ","0","1","2","3","4","5","6","7","8","9","%","?",'"','\n']

def removeEndBrackets(value):
    if len(value) > 1:
        if value[-2] == ')':
            openBracket = [i for i, x in enumerate(value) if x == '(']
            value = value[0:openBracket[-1]]
            value = str(removeEndSpace(value)) + '\n'
    return value

def removeEndSpace(value):
    if len(value) > 1:
        if value[-1] == ' ':
            value = removeEndSpace(value[0:len(value) - 1])
    return value

def sanitizer(src, out, lower):
    source = open(src,'r')
    #Clearing old output
    open(out,'w').close()
    output = open(out,'a')
    for line in source:
        #Clearing of weird symbols and brackets
        line = line.replace('\u2005',' ')
        line = removeEndBrackets(line)
        if lower:
            output.write(line.lower())
        else:
            output.write(line) 
    else:
        source.close()
        output.close()