[Intro]
Why you great 'til you gotta be great?
Woo!

[Verse 1]
I just took a DNA test, turns out I'm 100% that kid
Even when I'm crying crazy
Yeah, I got some problems, that's the human in me
Bling bling, then I solve 'em, that's the goddess in me
You coulda had a good friend, non-committal
Help you with your career just a little
You're 'posed to hold me down, but you're holding me back
And that's the sound of me not calling you back

[Chorus]
Why you great 'til you gotta be great?
Don't text me, tell it straight to my face
Best friend sat me down in the salon chair
Shampoo press, get you out of my hair
Fresh photos with the bomb lighting
New ones, like the Minnesota Vikings
Truth hurts, needed something more exciting
Bom bom bi dom bi dum bum bay

[Verse 2]
You tried to break my heart?
Oh, that breaks my heart
That you thought you ever had it
No, you ain't from the start
Hey, I'm glad you're back with your friends
I mean, who would ever hide this?
I will never, ever, ever, ever, ever want it like that
I put the sing in single
Ain't worried 'bout a ring on my finger
So you can tell your friend, "Shoot your shot" when you see him
It's okay, he already knows my feelings

[Chorus]
Why you great 'til you gotta be great? ('Til you gotta be great)
Don't text me, tell it straight to my face (Tell it straight to my face)
Best friend sat me down in the salon chair (Down in the salon chair)
Shampoo press, get you out of my hair
Fresh photos with the bomb lighting (Bomb lighting)
New ones, like the Minnesota Vikings (Minnesota Vikings)
Truth hurts, needed something more exciting (Yee)
Bom bom bi dom bi dum bum bay (Eh, yeah, yeah, yeah)

[Bridge]
I'ma call you back in a minute (Yeah, yeah)
I don't play tag, yeah, I been it (One time)
We don't deal with lies (Two times), we don't do goodbyes (Woo)
We just keep it pushing like ay-ay-ay

[Chorus]
Why you great 'til they gotta be great? ('Til you gotta be great)
Don't text me, tell it straight to my face (Tell it straight to my face)
Best friend sat me down in the salon chair (Down in the salon chair)
Shampoo press, get you out of my hair
Fresh photos with the bomb lighting (Bomb lighting)
New ones, like the Minnesota Vikings (Minnesota Vikings)
Truth hurts, needed something more exciting (Yee)
Bom bom bi dom bi dum bum bay (Eh, yeah, yeah, yeah)

[Outro]
With the bomb lighting
Minnesota Vikings
Yee, eh, yeah, yeah, yeah