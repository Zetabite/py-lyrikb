#!/usr/bin/env python
import json
import lyrikb_util as kbu

wordFilter = ["i","you","he","she","it","we","they"]
memo_lyrikb = []
memo_original = []

lyrikbLyricsSrc = "https://www.metrolyrics.com/kidz-bop-kids-lyrics.html"

def processFile(src):
    array = []
    source = open(src,'r')
    for line in source:
        array = array + processLine(line)
    else:
        source.close()
        return array

def processLine(source):
    array = []
    word = ""
    for c in source:
        if c not in kbu.charFilter:
            word = word + c
        else:
            if len(word) > 0: 
                array.append(str(word))
                word = ""
    else:
        word = word[0:len(word) - 1]
        if len(word) > 0:
            array.append(str(word))
            word = ""
        return array

def getChanges(original, lyrikb):
    changes = []

    if len(original) == len(lyrikb):
        for i in range(len(original)):
            if original[i] != lyrikb[i]: #and original[i].lower() not in wordFilter:
                changes.append([original[i],lyrikb[i]])
    else:
        print("Input needs further adjustment")
    return changes

def getMergedAlternatives(changes):
    alts = []
    done = []
    for f in changes:
        if f[0] not in done:
            temp = []
            for s in changes:
                if f[0] == s[0] and s[1] not in temp:
                    temp.append(s[1])
            else:
                alts.append([f[0],temp])
                done.append(f[0])
    return alts

def learn():
    kbu.sanitizer("original.txt", "original_sane.txt", True)
    kbu.sanitizer("lyrikb.txt", "lyrikb_sane.txt", True)

    memo_original = processFile("original_sane.txt")
    memo_lyrikb = processFile("lyrikb_sane.txt")

    changes = getChanges(memo_original, memo_lyrikb)
    merged = getMergedAlternatives(changes)

    data = {}
    for e in merged:
        data[e[0]] = []
        for f in e[1]:
            data[e[0]].append(f)
#    with open('data.json', 'w') as outfile:
#        json.dump(data, outfile)

    open("data.json",'w').close()
    output = open("data.json",'w')
    output.write(json.dumps(data, indent = 4, sort_keys=True))
    output.close()

#    with open('data.json') as json_file:
#        data = json.load(json_file)
#        for p in data['lol']:
#            print(p)

if __name__ == '__main__':
    learn()