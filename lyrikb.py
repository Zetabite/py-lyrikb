#!/usr/bin/env python
import json
import random
import lyrikb_util as kbu

def bopify(src):
    data = json.load(open('data.json'))
    source = open(src,'r')
    open("output.txt",'w').close()
    output = open("output.txt",'a')
    kbu.sanitizer(src, "temp.txt", False)
    source = open("temp.txt",'r')
    savedLines = []
    savedReplacement = []

    for line in source:
        bopifiedLine = ""
        word = ""
        if line in savedLines:
            position = [i for i, x in enumerate(savedLines) if x == line]
            bopifiedLine = savedReplacement[position[0]]
        else:
            for i in range(len(line)):
                if line[i] in kbu.charFilter:
                    bopifiedLine = bopifiedLine + getReplacement(word, data)
                    if line[i] != '\n':
                        bopifiedLine = bopifiedLine + line[i]
                    word = ""
                else:
                    word = word + line[i]
            else:
                bopifiedLine = kbu.removeEndSpace(bopifiedLine)
                savedLines.append(line)
                savedReplacement.append(bopifiedLine)
        output.write(bopifiedLine)
        print(bopifiedLine)
    else:
        output.close()
        source.close()

def getReplacement(value,data):
    if value.lower() in data.keys():
        replacement = str(random.choice(data[value.lower()]))
        if value[0].isupper():
            replacement = replacement[0].upper() + replacement[1:len(replacement)]
        return replacement
    else:
        return value

if __name__ == '__main__':
    bopify("lyrcis.txt")